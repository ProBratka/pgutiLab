package org.example.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Task1Test {
    private Task1 task1;

    @BeforeEach
    public void setUp() {
        task1 = new Task1();
    }

    @Test
    void task() {
        Assertions.assertEquals(1, task1.result());
    }
}